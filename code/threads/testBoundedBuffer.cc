#include "BoundedBuffer.h"
#include "testBoundedBuffer.h"
#include <stdio.h>

BoundedBuffer* pBuf;

int BoundedBufferTest()
{
	pBuf = new BoundedBuffer(MAX_BUF_SIZE);
	Thread* producer = new Thread("Producer1");
	Thread* consumer = new Thread("Consumer1");
	producer->Fork(Producer, 0);
	consumer->Fork(Consumer, 0);
}


static void Consumer(void* param)
{
	char data[MAX_BUF_SIZE];
	pBuf->Read(data,MAX_BUF_SIZE);
	for(int i = 0 ; i < MAX_BUF_SIZE ; i++)
	{
		printf("Consumer() - Read %c \n" , data[i]);
	}
}


static void Producer(void* param)
{
	char c = 'a';
	for(int i = 0 ; i < 10 ; i++)
	{
		pBuf->Write(&c,sizeof(char));
		printf("Producer() - Write %c \n", c);
		c++;
	}
}
